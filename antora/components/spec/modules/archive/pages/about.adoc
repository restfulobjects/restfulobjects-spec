= Archive

The Restful Objects specification was originally written using MS Word.
These are the original publications:

* link:{attachmentsdir}/v1.0/restfulobjects-spec.doc[restfulobjects-spec.doc] (v1.0)
* link:{attachmentsdir}/v1.0/restfulobjects-spec.pdf[restfulobjects-spec.pdf] (v1.0)

This website is equivalent to these documents.

An unfinished draft v1.1 also exists:

* link:{attachmentsdir}/v1.1-SNAPSHOT/restfulobjects-spec.doc[restfulobjects-spec.doc]
* link:{attachmentsdir}/v1.1-SNAPSHOT/restfulobjects-spec.pdf[restfulobjects-spec.pdf]
