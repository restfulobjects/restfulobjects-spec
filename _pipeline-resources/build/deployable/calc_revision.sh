#!/bin/bash

echo ""
echo ""
echo ""

if [ $# -ne 1 ]
then
    echo "usage: $(basename $0) OUTPUTDIR" >&2
    exit 1
fi

OUTPUTDIR=$1


DATE=$(date +%Y%m%d.%H%M)
BRANCH=$(echo $CI_COMMIT_BRANCH | sed 's|^rel/||g' | sed 's|[.]|_|g' | awk -F/ '{ print $NF }')
GIT_SHORT_COMMIT=$(echo $CI_COMMIT_SHA | cut -c1-8)

REVISION=$DATE.$BRANCH.$GIT_SHORT_COMMIT

echo "$REVISION" > $OUTPUTDIR/REVISION
